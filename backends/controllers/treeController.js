var Tree = require('../models/tree');
var mongoose = require('mongoose');

var db = mongoose.connection;

exports.trees_index = function(req,res){
    Tree.find({},{"name":1,"_id":1},(err,result)=>{
        if(err){
            res.send(err);
        }
        else{
            res.render('index',{treeList:result});
        }
    })
}

exports.new_tree = function(req,res){
    var posted_tree = new Tree(req.body);
    posted_tree.save(function(err, tree) {
        if (err){
           res.send(err);
        }
        else res.json(tree);
       });    
}

exports.show_tree = function(req,res){
    const details = { '_id' : mongoose.Types.ObjectId(req.params.id) };
    db.collection('trees').findOne(details, (err, item) => {
        if (err) {
            res.send({'error':'An error has occurred'});
        } else {
            res.render('show',{tree:JSON.stringify(item)});
        }
      });
}

exports.remove_tree = function(req,res){
    res.send('Can hear')
}