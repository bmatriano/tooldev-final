var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TreeSchema = new Schema({
    name:{
        type: String
    },
    created_at:{
        type: Date,
        default: Date.now
    },
    tree_proper:{
        type: Object,
        required: 'Please enter a tree'
    }

});

module.exports = mongoose.model('Trees',TreeSchema);