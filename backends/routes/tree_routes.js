var controller = require('../controllers/treeController');
var express = require('express');
var router = express.Router();

router.get('/',controller.trees_index);
router.get('/:id',controller.show_tree);
router.post('/new',controller.new_tree);
router.post('/:id/delete',controller.remove_tree);
router.get('/:id/delete',controller.remove_tree);


module.exports = router;