var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var pathfinderUI = require('pathfinder-ui')
require('dotenv').config()
var mongoDB = process.env.database;
var trees = require('./routes/tree_routes.js');
var app = express();

app.use(bodyParser.json({limit: '250mb'}));
app.use(bodyParser.urlencoded({limit: '250mb'}));
app.set('view engine','ejs')
app.set('views', './views')
mongoose.Promise = global.Promise;
mongoose.connect(mongoDB,(err,database)=>{
    if (err)
    {
        console.log(err)
    }
    else {
        console.log(database)
    }    
});
var db = mongoose.connection;

app.use('/css', express.static('semantic'));


app.use('/trees',trees);

app.use('/pathfinder', function(req, res, next){
	pathfinderUI(app)
	next()
}, pathfinderUI.router)

app.listen(process.env.express_port || 8000, ()=>{
    console.log('App running on '+process.env.express_port);
});