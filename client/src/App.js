import React, { Component } from 'react';
import './App.css';
import './semantic.css';
import { Question } from './Question';

class App extends Component {
  render() {
    return (
      <div className="App">
        <h1>
          Decision Tree
        </h1>
        <div id="tree_title" className="ui large input focus">
          <input type='text' placeholder="Decision tree name here"></input>
        </div>
        <br/>
        <hr/>
        <br/>
        <Question className="question_div" id="start_node" parent='null' />
      </div>
    );
  }
}

export default App;
