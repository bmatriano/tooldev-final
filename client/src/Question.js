import React from 'react';
import { Step } from './Step'
import './Question.css';
export class Question extends React.Component{
	constructor(props){
		super(props);
		this.state = {children: [], text:'', children_visible: true, file:null, child_values:[], child_tree: null};
		this.id = props.id;
		if(props.tree){
			this.setState({
				text: props.tree.text,
				child_tree: props.tree.children
			})
			let initial_children = [];
			for(let i=0; i<this.state.child_tree.length;i++){
				let new_addition = this.state.child_tree[i].children.length > 0 ? <Question tree={this.state.child_tree[i].children} /> : <Step text={this.state.child_tree[i].text} />;
				initial_children.push(new_addition);

			}
			this.setState({
				children: initial_children
			});
		}
		if (props.current_value != null){
			this.state.text=props.current_value;
		}
		this.counter = 0;
		this.addElement = this.addElement.bind(this);
		this.addEnd=this.addEnd.bind(this);
		this.removeElement = this.removeElement.bind(this);
		this.updateField = this.updateField.bind(this);
		this.hideChildren = this.hideChildren.bind(this);
		this.state.children = [];
	}
	
	updateField(event){
		this.setState({
			text: event.target.value  
		});
	}

	hideChildren(){
		this.setState({
			children_visible: (this.state.children_visible?false:true)
		})
	}

	addElement(){
		this.setState({
		  children: this.state.children.concat(<Question count = {this.counter} current_value={this.state.child_values[this.state.children.length]} />)
		});
		this.counter = this.counter+1;
	}

	addEnd(){
		this.setState({
			children: this.state.children.concat(<Step count={this.counter} />)
		});
		this.counter = this.counter+1;

	}

	removeElement(i){
		var new_children = this.state.children.slice();
		new_children.splice(i,1);
		this.setState({
			children: new_children
		});
	}

	render(){
		return(
		<div className="question_div">
	        <div className="ui small input focus">
				<input id={this.id} className="question" type="text" onChange={this.updateField} placeholder='Question' value={this.state.text}></input>
			</div>
			<br/>
			{this.id !== 'start_node' ? (
			<div className="ui small input focus">
				<input type="text" className="section-label" placeholder="Branch label"></input>
			</div>
			) : true }
			<ul id={this.id+'_list'}>
					{
						this.state.children.map((child,i)=>{
							return (
									<li code={i} style={this.state.children_visible?null:{display:'none'}} key={Math.random()*1000}>
											{child} 
											<button className="ui red button" onClick={()=>this.removeElement(i)}> 
												Remove 
											</button> 
									</li>
								);
						})
					}
			</ul>
			<button className="ui button" onClick={this.addElement}>Add Decision</button>
			<button className="ui button" onClick={this.addEnd}> Add Step </button>
			<input  id={this.id} className="ui button file" type="file" accept="image/*"></input>
		</div>
		);
	}
}