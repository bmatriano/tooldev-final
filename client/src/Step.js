import React from 'react';

export class Step extends React.Component{
	
	constructor(props){
		super(props);
		this.updateField = this.updateField.bind(this);
		this.state = {text:''}
		if(this.props.text){
			this.setState({
				text: this.props.text
			});
		}
	}

	updateField(event){
		this.setState({
			text: event.target.value  
		});
		console.log(this.state.text);
	}

	render(){
		return(
			<div>
				<div className="ui small input focus">
					<input type='text' onChange={this.updateField} value={this.state.text}/>
				</div>
				<br/>
				<div className="ui input focus">
					<input type="file" accept="image/*"/>
				</div>
				<br/>
			</div>
		);
	}	
}